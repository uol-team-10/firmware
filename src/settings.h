/**
 * @file settings.h
 * @author Oliver Newman (oliver@olivernewman.co.uk)
 * @brief Structs for holding settings data
 * @version 0.0.3
 * @date 2020-04-29
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef SETTINGS_H
#define SETTINGS_H

// SHOULD PROBABLY HAVE SET SIZES FOR THE char ARRAYS

// Details of an individual item on the settings menu
typedef struct
{
    const char *name;
    /* Whilst it is slightly redundant to have a set value stored alongside a 
     * range of values and the current index, this allows users to set precise 
     * options from the configuration page as well as being able to select a
     * value from the range on the device itself.
     */
    int value;
    const char *unit;
    int rangeIndex;
    int range[20];
    int rangeSize;
} Settings_Item;

// The overall structure of the changable items in the settings menu
typedef struct 
{
    // For confirming values are in flash memory.
    bool valid;
    Settings_Item buttonSensitivity;
    Settings_Item timeoutInterval;
    Settings_Item publishInterval;
} Changable_Settings;

// WiFi SSID and password
typedef struct
{
    const char *ssid;
    const char *pass;

} Wifi_Settings;

typedef struct
{
    const char *id;
    const char *authToken;
} Device_Settings;


typedef struct
{
    const char *url;
    const char *apiPath; // may not be needed/useful in this format, with the device id being required
    int port;
} Server_Settings;

#endif // SETTINGS_H