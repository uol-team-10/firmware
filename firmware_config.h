#ifndef FIRMWARE_CONFIG_H
#define FIRMWARE_CONFIG_H

#define SERIAL_PRINT    // Print output to serial monitor
// #define SERIAL_WAIT     // Wait for a serial monitor to be connected before starting
#define BAUD_RATE   9600

#define WIFI_DELAY  10000   // Time to wait between attempts to connect to wifi
#define RTC_SYNC    86400   // How often the real time clock should be synced

// Pins

// Sensors
#define DHT_PIN         2
#define LUX_PIN         A2
#define MOISTURE_PIN    A1

// RGB LED
#define R_PIN           3
#define G_PIN           4
#define B_PIN           5

// Buttons
#define BUTTON_TL_PIN   10
#define BUTTON_TR_PIN   9
#define BUTTON_BL_PIN   7
#define BUTTON_BR_PIN   8

#endif // FIRMWARE_CONFIG_H
