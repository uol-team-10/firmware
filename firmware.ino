/**
 * @file firmware.ino
 * @author Oliver Newman (oliver@olivernewman.co.uk)
 * @brief 
 * @version 0.0.9
 * @date 2020-04-24
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#include <WiFiNINA.h>
#include <ArduinoHttpClient.h>

#include <FlashStorage.h>
#include <Scheduler.h>

#include <RTCZero.h>

#include <DHT.h>

// Preprocessor definitions relating to pins, debugging. etc
#include "firmware_config.h"

#include "src/settings.h"

#include "src/telemetry/telemetry.h"
#include "src/temperature/temperature.h"
#include "src/humidity/humidity.h"
#include "src/lux/lux.h"
#include "src/rgb/rgb.h"
#include "src/moisture/moisture.h"

#include "src/screen/screen.h"

#include "src/menu/menu_state.h"

#include "src/menu/menu.h"
#include "src/menu/menu_startup.h"
#include "src/menu/menu_off.h"
#include "src/menu/menu_main.h"
#include "src/menu/menu_telemetry.h"
#include "src/menu/menu_settings.h"
#include "src/menu/menu_about.h"
#include "src/menu/menu_calibration.h"

// Includes sensitive hardcoded values such as wifi SSID and password. Used in
// development instead of values from flash memory.
#include "secrets.h" 

#define SETUP_MAX_PROGRESS              20
#define BUTTON_SENSITIVITY_CALIBRATION  1400
#define TELEMETRY_TIMEOUT_MULTIPLIER    10

/*
 * Arduino defines the main function within its core library. It calls the 
 * setup function below once, and runs the loop function indefinitely. The
 * declarations below are therefore essentially global variables, however, by 
 * including them in this file beflow the include statements, we essentially
 * limit their access to the main function. 
 */

/* 
 * FlashStorage defines areas of the program memory for use as storage. The
 * first argument is the variable name you want to use (does not require 
 * declaring in advance) and the second is the data structure being stored/the
 * function should expect to find at the location.
 */
FlashStorage(device_store, Device_Settings);
FlashStorage(server_store, Server_Settings);
FlashStorage(wifi_store, Wifi_Settings);
FlashStorage(settings_store, Changable_Settings);
FlashStorage(moisture_store, Moisture_Range);

// Creates a client that can connect to to a specified internet IP address and port
WiFiClient wifiClient;

// Library to make it easier to interact with web servers
HttpClient httpClient(wifiClient, SERVER_URL, SERVER_PORT);

// Library for handling the MKR WiFi 1010's built in real-time clock
RTCZero rtc;

// Tracks millis since last sync. Allows us to sync the RTC every 24 hours
u_long lastSync;

// Hardcoded values for the settable options in the settings menu whilst 
// in development.
Changable_Settings settings = {
    true,
//      name                value   unit    rangeIndex  range                                                           rangeSize
    {   "Button Response",  5,      "",     6,          { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 },                              10  }, 
    {   "Timeout Interval", 10,     "s",    1,          { 5, 10, 15, 20, 30, 45, 60 },                                  7   }, 
    {   "Publish Interval", 5,      "s",    2,          { 1, 2, 5, 10, 15, 20, 30, 45, 60, 120, 180, 240, 300, 600 },   14  } 
};

// WiFi settings from secrets for use during development
Wifi_Settings wifiSettings = { WIFI_SSID, WIFI_PASS };

// Wifi status enum declared in Arduino WiFiNINA library
int wifi_status = WL_IDLE_STATUS;

// Hardcoded moisture range initialisation values for development. Struct 
// defined in moisture.h
Moisture_Range moistureRange = {true, 800, 375};

// Sensor objects
DHT                 dht(DHT_PIN, DHT22);

TEMPERATURE         temperature(&dht);
HUMIDITY            humidity(&dht);
LUX                 lux(LUX_PIN);
MOISTURE            moisture(MOISTURE_PIN, moistureRange);

RGB                 rgb(R_PIN, G_PIN, B_PIN);

// Menu objects
MENU_STARTUP        menuStartup;
MENU_OFF            menuOff;
MENU_MAIN           menuMain;
MENU_TELEMETRY      menuTelemetry(&temperature, &humidity, &lux, &moisture);
MENU_SETTINGS       menuSettings(&settings);
MENU_ABOUT          menuAbout;
MENU_CALIBRATION    menuCalibration(&moisture);

MENU                *currentMenu;

SCREEN              screen;

menu_state          menuState;

int buttonPins[] = { 
    BUTTON_TL_PIN, 
    BUTTON_TR_PIN, 
    BUTTON_BL_PIN, 
    BUTTON_BR_PIN 
};

int buttonStates[4] = { 0 };

// Enum for the menu buttons
enum class button
{
    TOP_LEFT,
    TOP_RIGHT,
    BOTTOM_LEFT,
    BOTTOM_RIGHT
};

long timeoutMillis = 0;
long publishMillis = 0;
long buttonMillis = 0;

bool buttonPressed = false;

void setup()
{
    Serial.begin(BAUD_RATE);
    #ifdef SERIAL_WAIT
    while(!Serial);
    #endif

    screen.init();
    menuState = menu_state::MENU_STARTUP;
    
    menuStartup.setMaxProgress(SETUP_MAX_PROGRESS);
    menuStartup.setLoadText("Initialising WiFi");
    menuStartup.draw(&screen);
    Serial.println("Initialising WiFi...");
    wifi_setup();
    Serial.println("done");
    menuStartup.setLoadText("Initialising RTC");
    menuStartup.progress(2);
    menuStartup.draw(&screen);
    Serial.println("Initialising real time clock...");
    rtc.begin();
    
    menuStartup.setLoadText("Syncing RTC");
    menuStartup.progress(2);
    menuStartup.draw(&screen);
    rtc_sync();
    Serial.println("done");
    Serial.print("Initialising buttons...");
    menuStartup.setLoadText("Initialising Buttons");
    menuStartup.progress(2);
    menuStartup.draw(&screen);
    for (int i = 0; i < 4; i++)
        pinMode(buttonPins[i], INPUT);
    Serial.println("done");
    menuStartup.setLoadText("Initialising Sensors");
    menuStartup.progress(2);
    menuStartup.draw(&screen);
    Serial.print("Initialising sensors...");
    dht.begin();
    rgb.init();
    lux.init();
    moisture.init();
    Serial.println("done");
    menuStartup.setLoadText("Complete!");
    menuStartup.progress(SETUP_MAX_PROGRESS);
    menuStartup.draw(&screen);
    delay(1000);

    menuState = menu_state::MENU_OFF;
    currentMenu = &menuOff;

    currentMenu->draw(&screen);
    timeoutMillis = millis();

    Serial.println("Starting telemetry loop");
    Scheduler.startLoop(publishTelemetryLoop);
}

void loop()
{
    // Sync the rtc every 24 hours
    if (rtc.getEpoch() - lastSync > RTC_SYNC)
        rtc_sync();

    // Button control loop
    if (!buttonPressed)
    {
        for (int i = 0; i < 4; i++)
        {
            buttonStates[i] = digitalRead(buttonPins[i]);
            if (buttonStates[i])
            {
                buttonControl((button)i);
                
                buttonPressed = true;
                buttonMillis = millis();
                timeoutMillis = millis();
            }
        }
    } 
    else if (millis() - buttonMillis > BUTTON_SENSITIVITY_CALIBRATION / settings.buttonSensitivity.value)
        buttonPressed = false;

    // Menu timeout loop
    // If on the telemetry menu, increase the timeout value
    int timeoutInterval = (menuState == menu_state::MENU_TELEMETRY) ? 
        settings.timeoutInterval.value  * TELEMETRY_TIMEOUT_MULTIPLIER : 
        settings.timeoutInterval.value;

    // If screen is not off, check whether the timeout interval has been 
    // passed, and if so, turn off screen
    if (menuState != menu_state::MENU_OFF && 
        menuState != menu_state::CALIBRATION && 
        millis() - timeoutMillis > timeoutInterval * 1000)
    {
        menuState = menu_state::MENU_OFF;
        currentMenu = &menuOff;
        
        menuMain.reset();
    }

    currentMenu->draw(&screen);
    yield();    // Yields to the telemetry loop
}

void buttonControl(button b)
{   
    menu_state newState = menu_state::NO_CHANGE;
    switch (b)
    {
        case button::TOP_LEFT:
            newState = currentMenu->buttonTL();
            break;
        case button::BOTTOM_LEFT:
            newState = currentMenu->buttonBL();
            break;
        case button::TOP_RIGHT:
            newState = currentMenu->buttonTR();
            break;
        case button::BOTTOM_RIGHT:
            newState = currentMenu->buttonBR();
            break;
        default:
            break;
    }


    if (newState != menu_state::NO_CHANGE)
    {
        
        switch (newState)
        {
            case menu_state::MENU_MAIN:
                currentMenu = &menuMain;
                break;
            case menu_state::MENU_TELEMETRY:
                currentMenu = &menuTelemetry;
                currentMenu->reset();
                break;
            case menu_state::MENU_SETTINGS:
                currentMenu = &menuSettings;
                // If we're coming back from the calibration state, we don't 
                // want to reset the menu.
                if (menuState != menu_state::CALIBRATION)
                    currentMenu->reset();
                break;
            case menu_state::MENU_ABOUT:
                currentMenu = &menuAbout;
                currentMenu->reset();
                break;
            case menu_state::CALIBRATION:
                currentMenu = &menuCalibration;
                currentMenu->reset();
                break;
            default:
                break;
        }

        menuState = newState;
    }
}

void wifi_setup()
{
    while (wifi_status != WL_CONNECTED)
    {
        #ifdef SERIAL_PRINT
        Serial.print("Attempting to connect to SSID: ");
        Serial.println(wifiSettings.ssid);
        #endif

        wifi_status = WiFi.begin(wifiSettings.ssid, wifiSettings.pass);
        u_long timeCount = millis();
        u_long progressTime = 1000;
        int count = 0;
        while (millis() - count < 10000)
        {
            if (count < 10 && millis() - count > progressTime)
            {
                count++;
                menuStartup.progress();
                menuStartup.draw(&screen);
                progressTime += 1000;
            }
        }
    }
}

void rtc_sync()
{
    u_long epoch = 0;

    while (epoch == 0)
    {
        #ifdef SERIAL_PRINT
        // Serial.print("Wifi time: "); Serial.println(WiFi.getTime());
        #endif
        epoch = WiFi.getTime();
    }
    
    #ifdef SERIAL_PRINT
    Serial.print("epoch received: "); Serial.println(epoch);
    #endif

    rtc.setEpoch(epoch);
    lastSync = epoch;

    #ifdef SERIAL_PRINT
    Serial.print("rtc reading: ");
    Serial.println(rtc.getEpoch());
    Serial.print(rtc.getDay());
    Serial.print("/");
    Serial.print(rtc.getMonth());
    Serial.print("/");
    Serial.print(rtc.getYear());
    Serial.print(" ");
    Serial.print(rtc.getHours());
    Serial.print(":");
    Serial.print(rtc.getMinutes());
    Serial.print(":");
    Serial.println(rtc.getSeconds());
    #endif
}

void publish_telemetry()
{
    String contentType = "application/json";

    float t = temperature.getReading();
    float h = humidity.getReading();
    float l = lux.getReading();
    float m = moisture.getReading();
    ulong e = rtc.getEpoch();

    String telemetry = "{\"ts\":" + String(e) + ",";
    telemetry += "\"data\":[";
    telemetry += "{\"name\":\"temperature\",\"value\":" + String(t,1) + "},";
    telemetry += "{\"name\":\"humidity\",\"value\":" + String(h,1) + "},";
    telemetry += "{\"name\":\"lux\",\"value\":" + String(l,0) + "},";
    telemetry += "{\"name\":\"moisture\",\"value\":" + String(m,1) + "}";
    telemetry += "]}";

    // httpClient.post(API_PATH, contentType, telemetry);

    // int statusCode = httpClient.responseStatusCode();
    // Serial.print(statusCode);
    // Serial.print(" ");
    Serial.println(telemetry);
}

void publishTelemetryLoop()
{
    // Publish telemetry loop
    if (millis() - publishMillis > settings.publishInterval.value * 1000)
    {
        publish_telemetry();
        if (menuState == menu_state::MENU_TELEMETRY) currentMenu->draw(&screen);
        publishMillis = millis();
    }
    yield();
}